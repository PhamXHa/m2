<?php

namespace Bss\Testimonials\Block\Html;

class Pager extends \Magento\Theme\Block\Html\Pager
{
    /**
     * Current template name
     *
     * @var string
     */
    protected $_template = 'Bss_Testimonials::html/pager.phtml';
}
