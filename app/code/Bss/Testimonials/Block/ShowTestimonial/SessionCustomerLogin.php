<?php

namespace Bss\Testimonials\Block\ShowTestimonial;

use Magento\Framework\View\Element\Template;

class SessionCustomerLogin extends Template
{
  
    protected $coreSession;

    public function __construct(
            Template\Context $context,
            \Magento\Framework\Session\SessionManagerInterface $coreSession
        ){
            parent::__construct($context);
            $this->coreSession = $coreSession;
        }
    public function getSessionCustomerLogin()
    {   
        $customerLogin = $this->coreSession->getCheckLoginNow();
        $this->coreSession->unsCheckLoginNow();
        return $customerLogin;

        
    }    
}
