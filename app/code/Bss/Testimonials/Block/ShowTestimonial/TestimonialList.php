<?php

namespace Bss\Testimonials\Block\ShowTestimonial;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;
use Bss\Testimonials\Helper\Data;
use Bss\Testimonials\Block\Widget\Testimonial;

class TestimonialList extends Testimonial implements BlockInterface
{
    /**
     * @var integer
     */
    const DEFAULT_PER_PAGE_VALUE = 10;


    /**
     * @return $this
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if ($this->getTestimonialsCollection()) {
            $pager = $this->getLayout()->createBlock('Bss\Testimonials\Block\Html\Pager', 'bss.testimonials.list')
                // ->setAvailableLimit(array($limit=>$limit))
                ->setShowPerPage(true)
                ->setCollection($this->getTestimonialsCollection());
            $this->setChild('pager', $pager);
            $this->getTestimonialsCollection()->load();
        }
        return $this;
    }

    /**
     * Retrieve testimonials collection instance
     *
     * @return \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
     */
    public function getTestimonialsCollection()
    {
        $collection = $this->testimonialFactory->create()
            ->getCollection();
        $page = ($this->getRequest()->getParam('p')) ? $this->getRequest()->getParam('p') : 1;
        $collection->setPageSize(self::DEFAULT_PER_PAGE_VALUE);
        $collection->setCurPage($page);
        return $collection;
    }
}
