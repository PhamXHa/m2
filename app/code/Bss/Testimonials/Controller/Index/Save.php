<?php

namespace Bss\Testimonials\Controller\Index;

use Magento\Framework\Exception\LocalizedException;
use Magento\Customer\Model\Session;

class Save extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Bss\Testimonials\Helper\Data
     */
    protected $helper;

    /**
     * @var \Bss\Testimonials\Model\TestimonialFactory
     */
    protected $testimonialFactory;


    protected $orderCollectionFactory;

    protected $messageManager;


    protected $customerSession;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Bss\Testimonials\Helper\Data $helper
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Bss\Testimonials\Helper\Data $helper,
        \Bss\Testimonials\Model\TestimonialFactory $testimonialFactory
    ) {
        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->helper = $helper;
        $this->testimonialFactory = $testimonialFactory;
        parent::__construct($context);
        $this->messageManager = $messageManager;
        $this->customerSession=$customerSession;
    }

    /**
     * @return void
     */
    public function execute()
    {
        if ($this->checkConfigEnable()==1) {
            $customerId="";
            if ($this->getLoggedinCustomerId()) {
                $customerId=$this->getLoggedinCustomerId();
                $customerName= $this->getLoggedinCustomerName();
            }
            if (!$customerId=="") {
                $checkCustomer= false;
                $orderCollecion = $this->orderCollectionFactory
                ->create()
                ->addFieldToSelect('*');

                $orderCollecion->addAttributeToFilter('customer_id',
                    $customerId);
                if($orderCollecion->getSize()>0){
                    return $this->save($customerId,$customerName);
                }
                $this->messageManager->addError(__("You must have at least an order to are able to submit testimonial."));

            }
            else
            {
             $this->messageManager->addError(__("You must log in to are able to submit testimonial."));
         }

         $resultRedirect = $this->resultRedirectFactory->create();
         return $resultRedirect->setUrl($this->_redirect->getRefererUrl());
     }
     else
     {
        return $this->save();
    }
}

    public function checkConfigEnable(){
        $conf = $this->helper->isRestrictCustomerAddTestimonialEnabled();
        return $conf;
    }


    public function save($customerId=null ,$customerName=null)
    {
       /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
       $resultRedirect = $this->resultRedirectFactory->create();

       if (!$this->helper->isAllowedToAddTestimonial()) {
        $this->messageManager->addError(__('You are not allowed to add testimonial'));
        return $resultRedirect->setUrl($this->_redirect->getRefererUrl());
    }

    $data = $this->getRequest()->getPostValue();
    $data = array_merge($data,["customer_id"=>"$customerId"], ["customer_name"=>"$customerName"]);
    $model = $this->testimonialFactory->create();
    if ($data) {
        $imageRequest = $this->getRequest()->getFiles('image');
        $data = $this->helper->imageUpload($data, $imageRequest);
                // var_dump($data);
                // die();
        $model->setData($data);
        try {
            $model->save();
            $this->messageManager->addSuccess(__('Thank You for Adding Your Testimonial.'));
            return $resultRedirect->setUrl($this->_redirect->getRefererUrl());
        } catch (LocalizedException $e) {
            $this->messageManager->addError($e->getMessage());
        } catch (\Exception $e) {
            $this->messageManager->addException($e, __('Something went wrong while saving the testimonial.'));
        }
    }
    return $resultRedirect->setUrl($this->_redirect->getRefererUrl());
    }

    public function getLoggedinCustomerId() {

        $customerSession = $this->customerSession;

        if($customerSession->isLoggedIn()) {

            return $customerSession->getId();

        }
        return false;
    }
    public function getLoggedinCustomerName() {

        $customerSession = $this->customerSession;

        return $customerSession->getCustomer()->getName();
    }

}
