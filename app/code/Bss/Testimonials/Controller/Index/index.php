<?php

namespace Bss\Testimonials\Controller\Index;

use Magento\Framework\Exception\LocalizedException;

class Index extends \Magento\Framework\App\Action\Action
{

    public function execute()
    {
     $this->_view->loadLayout();
     $this->_view->renderLayout();
    }
}
