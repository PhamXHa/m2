<?php

namespace Bss\Testimonials\Observer;

use Magento\Framework\Event\ObserverInterface;

class CustomerLogin implements ObserverInterface
{
	protected $coreSession;

	public function __construct(
			\Magento\Framework\Session\SessionManagerInterface $coreSession
		){
			$this->coreSession = $coreSession;
		}
	public function execute(\Magento\Framework\Event\Observer $observer)
	{
		$this->coreSession->setCheckLoginNow(true);

		
	}
}