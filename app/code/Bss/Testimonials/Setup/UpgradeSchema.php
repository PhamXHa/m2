<?php

namespace Bss\Testimonials\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $upgrade = $setup;

        $upgrade->startSetup();

        /**
         * Create table 'bss_testimonials'
         */
        if (version_compare($context->getVersion(), '1.0.1') <0) {
            $table = $upgrade->getTable('bss_testimonials');
            if ($upgrade->getConnection()->isTableExists($table) == true) {
                $columns = [

                    'customer_id' => [

                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,

                        'nullable' => true,

                        'comment' => 'Customer Id',

                    ],
                    'customer_name' => [

                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,

                        'nullable' => true,

                        'comment' => 'Customer Name',

                    ]
                ];
            }
            foreach ($columns as $name => $definition) {

                $upgrade->getConnection()->addColumn($table, $name, $definition);


            }

        }

        $upgrade->endSetup();
    }
}
